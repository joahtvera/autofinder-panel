import { uploadImage } from "@/src/utils/cloudinary";
import { NextRequest, NextResponse } from "next/server";

export async function POST(request: NextRequest) {
    try {
        const data = await request.formData()
        const image = data.get('image') as File
        if (!data) throw new Error("Internal Server Error")
        const preBuffer = await image.arrayBuffer()
        const buffer = Buffer.from(preBuffer)
        const response = await uploadImage(buffer)
        return NextResponse.json({
            success: true,
            message: "Image uploaded succesfully",
            data: response
        })
    } catch (error) {
        console.log((error as Error).message)
        return NextResponse.json({
            success: false,
            message: (error as Error).message
        })
    }
}