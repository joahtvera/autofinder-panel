import VehicleForm from '@/src/features/vehicles/VehicleForm'
import BrandService from '@/src/services/brand'
import CategoryService from '@/src/services/category'
import LocationService from '@/src/services/location'
import React from 'react'

const page = async () => {
    const brands = await BrandService.get()
    const categories = await CategoryService.get()
    const locations = await LocationService.get()
    console.log(locations)
    return (
        <VehicleForm brands={brands.data ?? []} categories={categories.data ?? []} locations={locations.data ?? []}/>
    )
}

export default page