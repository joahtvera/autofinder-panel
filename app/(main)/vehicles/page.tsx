import VehiclesPage from '@/src/features/vehicles/VehiclesPage'
import VehicleService from '@/src/services/vehicle'

const page = async () => {
    const vehicles = await VehicleService.get()
    return (
        <VehiclesPage vehicles={vehicles.data ?? []}></VehiclesPage>
    )
}

export default page