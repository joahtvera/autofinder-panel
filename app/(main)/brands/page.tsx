import Header from '@/src/common/Header'
import React from 'react'

const page = () => {
    return (
        <section className='container mx-auto'>
            <Header>Brands</Header>
        </section>
    )
}

export default page