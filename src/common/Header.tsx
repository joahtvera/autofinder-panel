import { ReactNode } from 'react'

type Props = {
    children: ReactNode
}

const Header = ({ children }: Props) => {
    return (
        <header>
            <h1 className='text-2xl my-8 text-zinc-800'>
                {children}
            </h1>
        </header>
    )
}

export default Header