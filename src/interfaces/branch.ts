import { ILocation } from "./location"

export interface IBranch {
    _id: string
    name: string
    address: string
    location: ILocation
    createdAt?: Date
    updatedAt?: Date
}