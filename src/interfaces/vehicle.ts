export interface IVehicle {
    _id?: string
    brand?: string
    category?: string
    year?: number
    model?: string
    branch?: string
    imageUrl: string
    archived?: boolean
    createdAt?: Date
    updatedAt?: Date
}

export const DEFAULT_VEHICLE: IVehicle = {
    brand: '',
    category: '',
    year: 2024,
    imageUrl: '',
    archived: false,
    branch: '',
    model: ''
}