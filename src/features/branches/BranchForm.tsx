import Header from '@/src/common/Header'
import { IBranch } from '@/src/interfaces/branch'
import React from 'react'
import { Controller, useForm } from 'react-hook-form'

const BranchForm = () => {
    const {
        handleSubmit,
        formState: { errors },
        control,
        reset,
        getValues,
        setValue,
        setError,
        clearErrors
    } = useForm<IBranch>()

    const formHandler = () => {

    }
    
    return (
        <section className='container mx-auto max-w-md'>
            <Header>Add a Vehicle</Header>

            <form onSubmit={handleSubmit(formHandler)} className="outline outline-1 rounded-sm mb-8">
                <div className='grid grid-cols-2 gap-3 p-4'>

                    <div className='col-span-2'>
                        <label>Model:</label>
                        <Controller
                            name="name"
                            control={control}
                            rules={{ required: "Car's model required" }}
                            render={({ field }) => (
                                <input type='text' placeholder={"Type the car's model"} className='outline outline-1 rounded-sm p-2 w-full mt-1' value={field.value} onChange={field.onChange} />
                            )}
                        />
                        {
                            errors.name && <span className='text-red-500'>{errors.name.message}</span>
                        }
                    </div>

                    <div className='col-span-2'>
                        <label>Model:</label>
                        <Controller
                            name="address"
                            control={control}
                            rules={{ required: "Car's model required" }}
                            render={({ field }) => (
                                <input type='text' placeholder={"Type the car's model"} className='outline outline-1 rounded-sm p-2 w-full mt-1' value={field.value} onChange={field.onChange} />
                            )}
                        />
                        {
                            errors.address && <span className='text-red-500'>{errors.address.message}</span>
                        }
                    </div>

                    <button type="submit" className="outline outline-1 px-4 py-2 rounded-sm hover:bg-blue-100 col-span-2">Submit</button>
                </div>
            </form>
        </section>
    )
}

export default BranchForm