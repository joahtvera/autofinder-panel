import Header from '@/src/common/Header'
import { IVehicle } from '@/src/interfaces/vehicle'
import VehicleForm from './VehicleForm'

type Props = {
    vehicles: IVehicle[]
}

const VehiclesPage = ({ vehicles }: Props) => {
    return (
        <section className='container mx-auto'>
            <Header>Vehicles</Header>
            <div>
                {
                    vehicles.map((item) => (
                        <div key={item._id}>{item.model}</div>
                    ))
                }
            </div>
        </section>
    )
}

export default VehiclesPage