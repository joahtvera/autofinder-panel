'use client'
import Header from '@/src/common/Header'
import { IBrand } from '@/src/interfaces/brand'
import { ICategory } from '@/src/interfaces/category'
import { ILocation } from '@/src/interfaces/location'
import { DEFAULT_VEHICLE, IVehicle } from '@/src/interfaces/vehicle'
import UploadService from '@/src/services/upload'
import VehicleService from '@/src/services/vehicle'
import Image from 'next/image'
import React, { ChangeEvent, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'

type Props = {
    brands: IBrand[]
    categories: ICategory[]
    locations: ILocation[]
}

const VehicleForm = ({ brands, categories, locations }: Props) => {

    const [imageUrl, setImageUrl] = useState('')

    const {
        handleSubmit,
        formState: { errors },
        control,
        reset,
        getValues,
        setValue,
        setError,
        clearErrors
    } = useForm<IVehicle>({ defaultValues: DEFAULT_VEHICLE })

    const formHandler = async (data: IVehicle) => {
        console.log(data)
        if(!data) return
        try {
            const response = VehicleService.create(data)
        } catch (error) {
            console.log(error)
        }
    }

    const imageHandler = async (event: ChangeEvent<HTMLInputElement>) => {
        if (!event.target.files) return
        if (errors.imageUrl) clearErrors("imageUrl")
        const file = event.target.files[0]
        console.log(file.size)
        if (file.size > 102400) {
            event.target.files[0]
            return setError('imageUrl', { message: "The image must be less than 100 Kb. Choose another image." })
        }
        const formData = new FormData()
        formData.append('image', file, file.name)
        const response = await (await UploadService.upload(formData)).json()
        if (response.success) {
            const _imageUrl = response.data.secure_url as string
            setValue('imageUrl', _imageUrl)
            setImageUrl(_imageUrl)
        }
    }

    const getYears = () => {
        const optArr = []
        const currentYear = new Date().getFullYear()
        for (let i = 1990; i <= currentYear; i++) {
            optArr.push(i)
        }
        return optArr
    }

    return (
        <section className='container mx-auto max-w-md'>
            <Header>Add a Vehicle</Header>

            <form onSubmit={handleSubmit(formHandler)} className="outline outline-1 rounded-sm mb-8">
                <div className='grid grid-cols-2 gap-3 p-4'>
                    <div className='col-span-2'>
                        <label>Brand:</label>
                        <Controller
                            name="brand"
                            control={control}
                            rules={{ required: 'Choose a Car Brand' }}
                            render={({ field }) => (
                                <select className='outline outline-1 rounded-sm p-2 w-full mt-1' value={field.value} onChange={field.onChange}>
                                    {
                                        brands.map((item) => (
                                            <option key={item._id} value={item._id} >{item.name}</option>
                                        ))
                                    }
                                </select>
                            )}
                        />
                        {
                            errors.brand && <span className='text-red-500'>{errors.brand.message}</span>
                        }
                    </div>

                    <div className='col-span-2'>
                        <label>Category:</label>
                        <Controller
                            name="category"
                            control={control}
                            rules={{ required: 'Choose a Car Category' }}
                            render={({ field }) => (
                                <select className='outline outline-1 rounded-sm p-2 w-full mt-1' value={field.value} onChange={field.onChange}>
                                    {
                                        categories.map((item) => (
                                            <option key={item._id} value={item._id} >{item.name}</option>
                                        ))
                                    }
                                </select>
                            )}
                        />
                        {
                            errors.category && <span className='text-red-500'>{errors.category.message}</span>
                        }
                    </div>

                    <div className='col-span-2'>
                        <label>Year:</label>
                        <Controller
                            name="year"
                            control={control}
                            rules={{ required: 'Choose the car year' }}
                            render={({ field }) => (
                                <select className='outline outline-1 rounded-sm p-2 w-full mt-1' value={field.value} onChange={field.onChange}>
                                    {
                                        getYears().map((item) => (
                                            <option key={item} value={item} >{item}</option>
                                        ))
                                    }
                                </select>
                            )}
                        />
                        {
                            errors.year && <span className='text-red-500'>{errors.year.message}</span>
                        }
                    </div>

                    <div className='col-span-2'>
                        <label>Model:</label>
                        <Controller
                            name="model"
                            control={control}
                            rules={{ required: "Car's model required" }}
                            render={({ field }) => (
                                <input type='text' placeholder={"Type the car's model"} className='outline outline-1 rounded-sm p-2 w-full mt-1' value={field.value} onChange={field.onChange} />
                            )}
                        />
                        {
                            errors.model && <span className='text-red-500'>{errors.model.message}</span>
                        }
                    </div>

                    <div className='col-span-2'>
                        <label>Branch:</label>
                        <Controller
                            name="branch"
                            control={control}
                            rules={{ required: 'Choose a Car Branch' }}
                            render={({ field }) => (
                                <select className='outline outline-1 rounded-sm p-2 w-full mt-1' value={field.value} onChange={field.onChange}>
                                    {
                                        locations.map((item) => (
                                            <option key={item._id} value={item._id} >{item.name}</option>
                                        ))
                                    }
                                </select>
                            )}
                        />
                        {
                            errors.branch && <span className='text-red-500'>{errors.branch.message}</span>
                        }
                    </div>

                    <div className='col-span-2 flex flex-col'>
                        <label htmlFor='image'>Image:</label>
                        <Controller
                            name="imageUrl"
                            control={control}
                            rules={{ required: 'Choose the car image' }}
                            render={({ field }) => (
                                <input type="file" name="image" onChange={imageHandler} accept='image/*' multiple={false} className='mt-1'/>
                            )}
                        />
                        {
                            errors.imageUrl && <span className='text-red-500'>{errors.imageUrl.message}</span>
                        }
                    </div>

                    {
                        imageUrl &&
                        <div className='col-span-2 outline outline-1 p-1 rounded-sm'>
                            <Image src={imageUrl} width={400} height={200} alt='uploaded image' className='w-full h-48 object-contain' />
                        </div>
                    }

                    <button type="submit" className="outline outline-1 px-4 py-2 rounded-sm hover:bg-blue-100 col-span-2">Submit</button>
                </div>
            </form>
        </section>
    )
}

export default VehicleForm