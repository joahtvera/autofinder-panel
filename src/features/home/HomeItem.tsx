import Link from "next/link"
import { ReactNode } from "react"
import { Item } from "./itemsData"

type Props = {
    content: Item
    children: ReactNode
}

const HomeItem = ({ children, content }: Props) => {
    return (
        <Link href={content.route}>
            <div className='p-4 rounded-md outline outline-1 bg-white text-zinc-800 hover:shadow-lg hover:bg-red-50 hover:text-red-900 h-40 flex flex-col justify-between [&>h1]:hover:underline duration-150 ease-in-out'>
                <h1 className='uppercase'>{content.title}</h1>
                <p className='text-sm'>{content.description}</p>
                <div className='w-full flex justify-end'>
                    {children}
                </div>
            </div>
        </Link>
    )
}

export default HomeItem