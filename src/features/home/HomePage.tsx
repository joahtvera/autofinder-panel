import { TruckIcon, BuildingStorefrontIcon, MapPinIcon, ChatBubbleBottomCenterTextIcon, UserIcon, TagIcon, CubeIcon} from '@heroicons/react/24/outline'
import Header from '@/src/common/Header'
import HomeItem from './HomeItem'
import { items } from './itemsData'

const HomePage = () => {
    return (
        <section className='container mx-auto'>
            <Header>Welcome to Autofinder Panel</Header>
            <div className='grid grid-cols-3 gap-4'>

                <HomeItem content={items[0]}>
                    <TruckIcon className='w-6 h-6' />
                </HomeItem>

                <HomeItem content={items[1]}>
                    <BuildingStorefrontIcon className='w-6 h-6' />
                </HomeItem>

                <HomeItem content={items[2]}>
                    <MapPinIcon className='w-6 h-6' />
                </HomeItem>

                <HomeItem content={items[3]}>
                    <ChatBubbleBottomCenterTextIcon className='w-6 h-6' />
                </HomeItem>

                <HomeItem content={items[4]}>
                    <UserIcon className='w-6 h-6' />
                </HomeItem>

                <HomeItem content={items[5]}>
                    <TagIcon className='w-6 h-6' />
                </HomeItem>

                <HomeItem content={items[6]}>
                    <CubeIcon className='w-6 h-6' />
                </HomeItem>

            </div>
        </section>
    )
}

export default HomePage