export type Item = {
    title: string
    description: string
    route: string
}

export const items: Item[] = [
    {
        title: "Vehicles",
        description: "Explore and manage your fleet of vehicles.",
        route: "/vehicles",
    },
    {
        title: "Branches",
        description: "View and manage your branches or offices.",
        route: "/branches",
    },
    {
        title: "Locations",
        description: "Access and manage geographical locations associated with your branches.",
        route: "/locations",
    },
    {
        title: "Comments",
        description: "Review and respond to feedback or comments from users.",
        route: "/comments",
    },
    {
        title: "Users",
        description: "Manage user accounts and permissions within the application.",
        route: "/users",
    },
    {
        title: "Categories",
        description: "Manage user accounts and permissions within the application.",
        route: "/categories",
    },
    {
        title: "Brands",
        description: "Manage user accounts and permissions within the application.",
        route: "/brands",
    },
]