import { IBrand } from "@/src/interfaces/brand"
import { HeaderProps, HttpRequest } from "./api"

const BrandService = {
    get: async (props?: HeaderProps) => {
        props = { ...props, cache: 'no-store' }
        const response = await HttpRequest.get<IBrand[]>(`/api/brands`, props)
        return response
    },

    getById: async (id: string, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store' }
        return await HttpRequest.get<IBrand>(`/api/brands/${id}`, props)
    },

    create: async (brand: IBrand, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store', body: brand }
        return await HttpRequest.post(`/api/brands`, props)
    },

    delete: async (id: string, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store' }
        return await HttpRequest.delete(`/api/brands/${id}`, props)
    },

    update: async (brand: IBrand, id: string, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store', body: brand }
        return await HttpRequest.patch(`/api/brands/${id}`, props)
    }
}
export default BrandService