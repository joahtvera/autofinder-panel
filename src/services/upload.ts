import { HeaderProps, HttpRequest } from "./api"

const UploadService = {
    upload: async (data: FormData, props?: HeaderProps) => {
        return await fetch("/api/upload", {
            method: 'POST',
            body: data
        })
    },
}

export default UploadService