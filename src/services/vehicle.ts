import { IVehicle } from "@/src/interfaces/vehicle"
import { HeaderProps, HttpRequest } from "./api"

const VehicleService = {
    get: async (props?: HeaderProps) => {
        props = { ...props, cache: 'no-store' }
        const response = await HttpRequest.get<IVehicle[]>(`/api/vehicles`, props)
        return response
    },

    getById: async (id: string, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store' }
        return await HttpRequest.get<IVehicle>(`/api/vehicles/${id}`, props)
    },

    create: async (vehicle: IVehicle, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store', body: vehicle }
        return await HttpRequest.post(`/api/vehicles`, props)
    },

    delete: async (id: string, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store' }
        return await HttpRequest.delete(`/api/vehicles/${id}`, props)
    },

    update: async (vehicle: IVehicle, id: string, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store', body: vehicle }
        return await HttpRequest.patch(`/api/vehicles/${id}`, props)
    }
}
export default VehicleService