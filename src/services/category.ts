import { ICategory } from "@/src/interfaces/category"
import { HeaderProps, HttpRequest } from "./api"

const CategoryService = {
    get: async (props?: HeaderProps) => {
        props = { ...props, cache: 'no-store' }
        const response = await HttpRequest.get<ICategory[]>(`/api/categories`, props)
        return response
    },

    getById: async (id: string, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store' }
        return await HttpRequest.get<ICategory>(`/api/categories/${id}`, props)
    },

    create: async (category: ICategory, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store', body: category }
        return await HttpRequest.post(`/api/categories`, props)
    },

    delete: async (id: string, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store' }
        return await HttpRequest.delete(`/api/categories/${id}`, props)
    },

    update: async (category: ICategory, id: string, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store', body: category }
        return await HttpRequest.patch(`/api/categories/${id}`, props)
    }
}
export default CategoryService