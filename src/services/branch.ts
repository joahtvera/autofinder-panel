import { IBranch } from "@/src/interfaces/branch"
import { HeaderProps, HttpRequest } from "./api"

const BranchService = {
    get: async (props?: HeaderProps) => {
        props = { ...props, cache: 'no-store' }
        const response = await HttpRequest.get<IBranch[]>(`/api/branches`, props)
        return response
    },

    getById: async (id: string, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store' }
        return await HttpRequest.get<IBranch>(`/api/branches/${id}`, props)
    },

    create: async (branch: IBranch, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store', body: branch }
        return await HttpRequest.post(`/api/branches`, props)
    },

    delete: async (id: string, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store' }
        return await HttpRequest.delete(`/api/branches/${id}`, props)
    },

    update: async (branch: IBranch, id: string, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store', body: branch }
        return await HttpRequest.patch(`/api/branches/${id}`, props)
    }
}
export default BranchService