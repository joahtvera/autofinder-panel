export const BASE_URL = process.env.API_URL

console.log(BASE_URL)

export type ResponseType<T> = {
    success: boolean
    data?: T
    message?: string
}

type WithoutBodyMethods = 'GET' | 'DELETE'
type WithBodyMethods = 'POST' | 'PUT' | 'PATCH'
type HttpMethods = WithBodyMethods | WithoutBodyMethods
export type Params = Record<string, string>
type HttpRequestProps = RequestInit & { params?: Params }
type WithoutBodyProps = Omit<HttpRequestProps, 'body'>

export type HeaderProps = {
    cache?: RequestCache | undefined; credentials?: RequestCredentials | undefined;
    headers?: HeadersInit | undefined; integrity?: string | undefined; keepalive?: boolean | undefined;
    method?: string | undefined; mode?: RequestMode | undefined; redirect?: RequestRedirect | undefined;
    referrer?: string | undefined; referrerPolicy?: ReferrerPolicy | undefined;
    signal?: AbortSignal | null | undefined; window?: null | undefined; body?: any;
    next?: NextFetchRequestConfig | undefined; params?: Params | undefined
}

export const HttpRequest = {
    get: async <T>(url: string, props: WithoutBodyProps) => {
        return HttpRequest._request<T>(url, 'GET', { ...props })
    },
    post: async <T>(url: string, props: HttpRequestProps) => {
        return HttpRequest._requestBody<T>(url, 'POST', { ...props })
    },
    put: async <T>(url: string, props: HttpRequestProps) => {
        return HttpRequest._requestBody<T>(url, 'PUT', { ...props })
    },
    patch: async <T>(url: string, props: HttpRequestProps) => {
        return HttpRequest._requestBody<T>(url, 'PATCH', { ...props })
    },
    delete: async <T>(url: string, props: WithoutBodyProps) => {
        return HttpRequest._request<T>(url, 'DELETE', { ...props })
    },
    _request: async <T extends unknown>(url: string, method: HttpMethods, props: HttpRequestProps): Promise<ResponseType<T>> => {
        try {
            const params = new URLSearchParams(props.params);
            // let fullUrl: string = `${props.headers ? (JSON.stringify(props.headers).includes("Authorization") ? BASE_URL : '') : BASE_URL}${url}${params.size ? `?${params}` : ''}`;
            let fullUrl: string = `${BASE_URL}${url}`
            const data = await fetch(fullUrl, { method: method, ...props })
            if (!data.ok) {
                throw new Error(data.statusText)
            }
            // const json = await data.json() as T
            // return {
            //     success: true, data: json
            // }
            return await data.json()
        } catch (error) {
            return {
                success: false, message: (error as Error).message
            }
        }
    },
    _requestBody: async <T extends unknown>(url: string, method: WithBodyMethods, props: HttpRequestProps): Promise<ResponseType<T>> => {
        return HttpRequest._request<T>(url, method, {
            headers: {
                'Content-Type': 'application/json',
                ...props.headers
            }, ...props, body: JSON.stringify(props.body)
        })
    }
}