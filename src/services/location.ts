import { ILocation } from "@/src/interfaces/location"
import { HeaderProps, HttpRequest } from "./api"

const LocationService = {

    get: async (props?: HeaderProps) => {
        props = { ...props, cache: 'no-store' }
        const response = await HttpRequest.get<ILocation[]>(`/api/locations`, props)
        return response
    },

    getById: async (id: string, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store' }
        return await HttpRequest.get<ILocation>(`/api/locations/${id}`, props)
    },

    create: async (location: ILocation, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store', body: location }
        return await HttpRequest.post(`/api/locations`, props)
    },

    delete: async (id: string, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store' }
        return await HttpRequest.delete(`/api/locations/${id}`, props)
    },

    update: async (location: ILocation, id: string, props?: HeaderProps) => {
        props = { ...props, cache: 'no-store', body: location }
        return await HttpRequest.patch(`/api/locations/${id}`, props)
    }
}

export default LocationService